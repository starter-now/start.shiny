library(shiny)
ui <- fluidPage(
  tags$head(
    tags$style(HTML("
  
  .column_w_bar {
      border-color: black;
      border-width: 1px;
      border-style: solid;
}
") # end HTML
    ) # end tags$style
  ), # end tags$head

  titlePanel("Covid tracker"),
  
  sidebarLayout(
    sidebarPanel("Interface"),
    mainPanel("Panneau principal",
              fluidRow(
                column(4,"colonne 1.1",class = 'column_w_bar'),
                column(8,"colonne 1.2",class = 'column_w_bar')
              ),
              fluidRow(
                column(6,"colonne 2.1",class = 'column_w_bar'),
                column(6,"colonne 2.2",class = 'column_w_bar',
                       fluidRow("ligne 1"),
                       fluidRow("ligne 2")
                )
              )
    )
  )
)
